/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shape;

/**
 *
 * @author a
 */
public class Sqaure extends Shape {

    private double side;

    public Sqaure(double side) {
        super("Sqaure");
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    /**
     *
     * @return
     */
    @Override
    public double calArea() {
        return side * side;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Sqaure{" + "side = " + side + '}';
    }

}
